REST Coding Challenge:
The objective of this code challenge is to write a small application which Creates, Reads, Updates, and Deletes waste profiles. The app should be written in PHP, and should utilize the RESTful service documented below.

Rest Resource Endpoints:

	http://api.1bg.com/profile
	http://api.1bg.com/profile/{id}

Waste Profile JSON Example:

	{  
	   "generatorFirstName": "Jane",
	   "generatorLastName": "Doe",
	   "facilityName": "Elk River Landfill",
	   "wasteType": "Asbestos",
	   "wasteAmount": 30,
	   "wasteAmountUnit": "lbs"
	}

Create Profile Example:

	curl -X POST -i -H "Content-type: application/json" -d '{"generatorFirstName": "Jane","generatorLastName": "Doe", "facilityName": "Testerino", "wasteType": "Asbestos", "wasteAmount": 30, "wasteAmountUnit": "lbs"}' http://api.1bg.com/profile

Read Profiles Example:

	curl -X GET -i -H "Accept: application/json" http://api.1bg.com/profile

Read Profile Example:
	curl -X GET -i -H "Accept: application/json" http://api.1bg.com/profile/1

Update Profile Example:

	curl -X PUT -d '{"generatorFirstName": "Jerry","generatorLastName": "Garcia", "facilityName": "Elk River Landfill", "wasteType": "Diesel Residue", "wasteAmount": 500, "wasteAmountUnit": "tons"}' -i -H "Content-type: application/json" http://api.1bg.com/profile/14

Update Profile Field Example:

	curl -X PATCH -d '{"generatorFirstName": "Terry"}' -i -H "Content-type: application/json" http:/api.1bg.com/profile/14

Delete Profile Example:

	curl -X DELETE -i -H "Accept: application/json" http://api.1bg.com/profile/1