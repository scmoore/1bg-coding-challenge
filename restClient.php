<?php
class restClient {

  public $domain = "http://api.1bg.com/profile"; 
  
  public function __construct() {
  }

  public function curlExecWithOpts($uri, $header, $method = "GET", $content = '') {
    $ch = curl_init();
    
    // curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_URL, $uri);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    
    if ($content != '') {
      curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
      $header[] = "Content-length: " . strlen($content);
    
    } 
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    
    if ($method == "POST") {
      curl_setopt($ch, CURLOPT_POST, true);
    }

    $response = curl_exec($ch);
    $errorno = curl_errno($ch);
    $error = curl_error($ch);
    $status = curl_getinfo($ch,CURLINFO_HTTP_CODE);
    curl_close($ch);
    return $response;
    // echo "URI: " . $uri . "<br>";
    // echo "Response: " . $response . "<br>";
    // echo "Error: " . $error . "<br>";
    // echo "ErrorNo: " . $errorno . "<br>";
    // echo "Status: " . $status . "<br>";
  }

  public function readProfiles() {
    $header = array();
    $header[] = "Accept: application/json";
    $method = "GET";
    $uri = $this->domain;
    return $this->curlExecWithOpts($uri, $header, $method);
  }

  public function readProfile($profileID) {
    $header = array();
    $header[] = "Accept: application/json";
    $method = "GET";
    $uri = $this->domain . "/" . $profileID;
    
    return $this->curlExecWithOpts($uri, $header, $method);
  }

  public function createProfile($profileJSON) {
    $header = array();
    $header[] = "Content-type: application/json";
    $method = "POST";
    $uri = $this->domain;
    return $this->curlExecWithOpts($uri, $header, $method, $profileJSON);
  }

  public function updateProfile($profileID, $profileJSON) {
    $header = array();
    $header[] = "Content-type: application/json";
    $method = "PUT";
    $uri = $this->domain . "/" . $profileID;
    return $this->curlExecWithOpts($uri, $header, $method, $profileJSON);
  }
  // TODO might want to write a helper for k:v pair
  public function updateProfileField($profileID, $profileJSON) {
    $header = array();
    $header[] = "Content-type: application/json";
    $method = "PATCH";
    $uri = $this->domain . "/" . $profileID;
    return $this->curlExecWithOpts($uri, $header, $method, $profileJSON);
  }

  public function deleteProfile($profileID) {
    $header = array();
    $header[] = "Accept: application/json";
    $method = "DELETE";
    $uri = $this->domain . "/" . $profileID;
    return $this->curlExecWithOpts($uri, $header, $method);
  }

}

$client = new restClient();

if (isset($_POST)) {

  $json = json_decode($_POST["data"]);
 
  switch ($json->action) {
    case 'create':
      // don't pass action to API
      unset($json->action);
      echo $client->createProfile(json_encode($json));
      break;
    
    case 'read':
      if ($json->profile == -1) {
        echo $client->readProfiles();
      } else {
        echo $client->readProfile($json->profile);
      }
      break;

    case 'updateSome':
      unset($json->action);
      $id = $json->profile;
      unset($json->profile);

      $response = '';
      foreach ($json as $key=>$value) {
        $fields = array();
        $fields[$key] = $value;
        if ($key && $value) {
          $response .= $client->updateProfileField($id, json_encode($fields)) . "<br>";
        }
      }
      echo $response;
      break;

    case 'updateAll':
      unset($json->action);
      $id = $json->profile;
      unset($json->profile);
      $fields = array();
      foreach ($json as $key=>$value) {
        $fields[$key] = $value;
      }
      echo $client->updateProfile($id, json_encode($fields)) . "<br>";
      break;

    case 'delete':
      echo $client->deleteProfile($json->profile);
      break;
    
    default:
    break;
  }
}

?>